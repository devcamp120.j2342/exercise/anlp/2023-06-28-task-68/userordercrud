package com.devcamp.user_order_crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user_order_crud.model.COrder;
import com.devcamp.user_order_crud.service.OrderService;
import com.devcamp.user_order_crud.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    @GetMapping("/order/all")
    public ResponseEntity<Object> getAllOrders() {
        return orderService.getAllOrderSV();
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(value="id") long id) {
        return orderService.getOrderByIdSV(id);
    }

    @GetMapping("/users/{id}/orders")
    public ResponseEntity<List<COrder>> getOrdersByUser(@PathVariable(value="id") Long id) {
        List<COrder> orders = userService.getOrderByUser(id);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @PostMapping("/order/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder orderCilent) {
        return orderService.createdOrderSV(orderCilent);
    }

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(value="id") long id, COrder orderUpdate) {
        return orderService.updateOrderSv(id, orderUpdate);
    }

    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable(value="id") long id) {
        return orderService.deleteOrder(id);
    }
}

