package com.devcamp.user_order_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserOrderCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserOrderCrudApplication.class, args);
	}

}
