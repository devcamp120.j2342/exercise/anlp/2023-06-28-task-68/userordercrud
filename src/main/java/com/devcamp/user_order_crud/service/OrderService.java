package com.devcamp.user_order_crud.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.user_order_crud.model.COrder;
import com.devcamp.user_order_crud.repository.ICorderRepository;


@Service
public class OrderService {
    @Autowired
    ICorderRepository orderRepository;

    public ResponseEntity<Object> getAllOrderSV() {
        try {
            List<COrder> orders = orderRepository.findAll();
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getOrderByIdSV(long id) {
        try {
            Optional <COrder> order = orderRepository.findById(id);
            if(order.isPresent()) {
                return new ResponseEntity<>(order.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<COrder> getOrderByUser(Long id) {
        return orderRepository.getOrderBycUserId(id);
    }

    public ResponseEntity<Object> createdOrderSV(COrder orderCilent) {
        try {
            COrder order = new COrder();
            order.setOrderCode(orderCilent.getOrderCode());
            order.setPizzaSize(orderCilent.getPizzaSize());
            order.setPizzaType(orderCilent.getPizzaType());
            order.setPrice(orderCilent.getPrice());
            order.setUpdated(null);
            orderRepository.save(order);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> updateOrderSv(long id, COrder orderUpdate) {
        try {
            Optional<COrder> orderData = orderRepository.findById(id);
            if(orderData.isPresent()) {
                COrder order = orderData.get();
                order.setOrderCode(orderUpdate.getOrderCode());
                order.setPizzaSize(orderUpdate.getPizzaSize());
                order.setPizzaType(orderUpdate.getPizzaType());
                order.setPrice(orderUpdate.getPrice());

                orderRepository.save(order);
                return new ResponseEntity<>(order, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteOrder(long id) {
        try {
            Optional <COrder> order = orderRepository.findById(id);
            if(order.isPresent()) {
                orderRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

