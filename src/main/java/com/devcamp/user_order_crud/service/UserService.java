package com.devcamp.user_order_crud.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.user_order_crud.model.COrder;
import com.devcamp.user_order_crud.model.CUser;
import com.devcamp.user_order_crud.repository.ICorderRepository;
import com.devcamp.user_order_crud.repository.ICuserRepository;

@Service
public class UserService {
    @Autowired
    ICuserRepository userRepository;

    @Autowired
    ICorderRepository orderRepository;

    public ResponseEntity<Object> getAllUserSV() {
        try {
            List<CUser> userList = new ArrayList<>();
            userRepository.findAll().forEach(userList::add);
            return new ResponseEntity<>(userList, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getUserByIdSV(long id) {
        try {
            Optional <CUser> optionalUser = userRepository.findById(id);
            if(optionalUser.isPresent()) {
                return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> createUserSV( CUser cUserCilent) {
        try {
            CUser user = new CUser();
            user.setAddress(cUserCilent.getAddress());
            user.setEmail(cUserCilent.getEmail());
            user.setFullname(cUserCilent.getFullname());
            user.setPhone(cUserCilent.getPhone());
            user.setUpdated(null);
            userRepository.save(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> updateUserSV(long id, CUser cUserUpdate) {
        Optional<CUser> optionalUser = userRepository.findById(id);
        try {
            if(optionalUser.isPresent()) {
                CUser user = optionalUser.get();
                user.setFullname(cUserUpdate.getFullname());
                user.setAddress(cUserUpdate.getAddress());
                user.setEmail(cUserUpdate.getEmail());
                user.setPhone(cUserUpdate.getPhone());
                userRepository.save(user);
                return new ResponseEntity<>(user, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteUser(long id) {
        try {
            Optional<CUser> user = userRepository.findById(id);
            if(user.isPresent()) {
                userRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e) {
            return ResponseEntity.unprocessableEntity().body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
        }
    }

    public List<COrder> getOrderByUser(Long id) {
        return orderRepository.getOrderBycUserId(id);
    }
}
