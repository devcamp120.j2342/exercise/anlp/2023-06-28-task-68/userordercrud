package com.devcamp.user_order_crud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user_order_crud.model.COrder;

public interface ICorderRepository extends JpaRepository<COrder, Long>{
    List<COrder> getOrderBycUserId(Long id);
}
