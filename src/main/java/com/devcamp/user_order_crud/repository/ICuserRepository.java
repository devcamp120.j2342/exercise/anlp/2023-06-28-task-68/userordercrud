package com.devcamp.user_order_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user_order_crud.model.CUser;

public interface ICuserRepository extends JpaRepository<CUser, Long>{
}
